from django.urls import path
from . import views


urlpatterns = [
    path('',views.index, name = 'index'),
    path('category',views.category_list, name = 'category'),
    path('products',views.product_list, name = 'products'),
    path('product_details/<slug>', views.product_details, name = 'product-details'),
    path('category_products/<slug>', views.category_products, name = 'category-products'),
    path('contat', views.contact, name = 'contact'),


    #cart urls
    path('cart/add/<int:id>/', views.cart_add, name='cart_add'),
    path('cart/item_clear/<int:id>/', views.item_clear, name='item_clear'),
    path('cart/item_increment/<int:id>/',
         views.item_increment, name='item_increment'),
    path('cart/item_decrement/<int:id>/',
         views.item_decrement, name='item_decrement'),
    path('cart/cart_clear/', views.cart_clear, name='cart_clear'),
    path('cart/cart-detail/',views.cart_detail,name='cart_detail'),
]